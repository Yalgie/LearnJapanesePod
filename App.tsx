import React, { Component } from 'react';
import {
  AppState,
  DataStore,
  Podcast,
  PodcastTypes,
  Sort,
} from './components/types';
import * as FileSystem from 'expo-file-system';
import { AudioPlayer } from './components/AudioPlayer';
import { fetchRss } from './utils/fetchRss';
import { storeData, retrieveData, deleteRssCache } from './utils/dataStore';
import { readDir, makeDir, deleteDir } from './utils/dir';
import { filterRss } from './utils/filter';
import { Nav } from './components/Navigation';

export class App extends Component<null, AppState> {
  state = {
    rss: undefined,
    loadingRss: true,
    announcements: undefined,
    loadingRssAnnouncements: true,
    audio: undefined,
    filter: {
      fun: false,
      lessons: true,
      starred: true,
      sort: Sort.ASC,
    },
  };

  getRss = async () => {
    const rss = await fetchRss('http://feeds.feedburner.com/learnjapanesepod');
    return rss;
  };

  // listenForConnectionChanges = () => {
  //   NetInfo.addEventListener(state => {
  //     // console.log('Connected', state.isConnected);
  //   });
  // };

  componentDidMount = async () => {
    const { filter } = this.state;
    const storedFilterData = await retrieveData(DataStore.FILTER);
    const storedRssData = await retrieveData(DataStore.RSS);
    const fetchedRssData = storedRssData ? null : await this.getRss();

    const hasDownloadDir = await readDir('downloads');
    if (!hasDownloadDir) await makeDir('downloads');

    // await deleteRssCache(DataStore.RSS);
    // await deleteRssCache(DataStore.FILTER);
    // await deleteDir('downloads');

    this.setState(
      {
        ...this.state,
        rss: JSON.parse(storedRssData) || fetchedRssData,
        filter: JSON.parse(storedFilterData) || filter,
        loadingRss: false,
      },
      () => {
        const { rss, filter } = this.state;
        storeData(DataStore.RSS, JSON.stringify(JSON.stringify(rss)));
        storeData(DataStore.FILTER, JSON.stringify(JSON.stringify(filter)));
      }
    );

    // this.listenForConnectionChanges();
  };

  downloadCallback = downloadProgress => {
    const progress =
      downloadProgress.totalBytesWritten /
      downloadProgress.totalBytesExpectedToWrite;
    console.log(progress);
  };

  handleDownload = async (url: string, id: string) => {
    const { rss } = this.state;
    let item: Podcast | undefined = undefined;

    rss.filter(podcast => {
      if (podcast.id === id) {
        item = podcast;
      }
      if (podcast.notes && podcast.notes.id === id) {
        item = podcast.notes;
      }
      if (podcast.drill && podcast.drill.id === id) {
        item = podcast.drill;
      }
    });

    const { type, lessonRef } = item;
    const ext = type === PodcastTypes.PDF ? '.pdf' : '.mp3';
    const filename = `podcast_${lessonRef}_${type.toLocaleLowerCase()}${ext}`;
    const downloadResumable = FileSystem.createDownloadResumable(
      url,
      FileSystem.documentDirectory + 'downloads/' + filename,
      {},
      this.downloadCallback
    );

    try {
      const { uri } = await downloadResumable.downloadAsync();
      console.log('Finished downloading to ', uri);

      const updatedRss = rss.map(it => {
        if (it.id === id) {
          it.downloaded = uri;
        }
        if (it.notes && it.notes.id === id) {
          it.notes.downloaded = uri;
        }
        if (it.drill && it.drill.id === id) {
          it.drill.downloaded = uri;
        }

        return it;
      });

      this.setState(
        {
          ...this.state,
          rss: updatedRss,
        },
        () => {
          storeData(DataStore.RSS, JSON.stringify(JSON.stringify(updatedRss)));
        }
      );
    } catch (e) {
      console.error(e);
    }
  };

  handleStar = (starred: boolean, id: string): void => {
    const { rss } = this.state;

    const updatedRss = rss.map(item => {
      if (item.id === id) {
        item.starred = starred;
      }
      return item;
    });

    this.setState(
      {
        ...this.state,
        rss: updatedRss,
      },
      () => {
        storeData(DataStore.RSS, JSON.stringify(JSON.stringify(updatedRss)));
      }
    );
  };

  playNewAudio = (src: string) => {
    this.setState({
      ...this.state,
      audio: src,
    });
  };

  handleFilter = () => {
    const { filter } = this.state;
    const { sort } = filter;

    const updatedFilter = {
      ...filter,
      sort: sort === Sort.ASC ? Sort.DESC : Sort.ASC,
    };

    this.setState(
      {
        ...this.state,
        filter: updatedFilter,
      },
      () => {
        storeData(
          DataStore.FILTER,
          JSON.stringify(JSON.stringify(updatedFilter))
        );
      }
    );
  };

  handleRssRefresh = () => {
    console.log('REFRESH');
  };

  render() {
    const { loadingRss, audio } = this.state;
    const filteredRss = filterRss(this.state);

    return (
      <>
        <Nav
          filteredRss={filteredRss}
          loadingRss={loadingRss}
          handleRssRefresh={this.handleRssRefresh}
          playNewAudio={this.playNewAudio}
          handleDownload={this.handleDownload}
          handleStar={this.handleStar}
          handleFilter={this.handleFilter}
        />
        <AudioPlayer audio={audio} />
      </>
    );
  }
}

export default App;
