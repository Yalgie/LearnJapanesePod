# LearnJapanesePod

*Documentation In Progress*

## Stack
- Expo
- React Native
- Styled Components
- Storybook

## Dependencies
To run the app locally in your dev environment you'll need Xcode and Android Studio installed, Xcode is only available on MacOS so you won't be able to run the iOS sim on Windows.

Alternatively you can run the app on the Expo app by downloading it on the App or Play stores.

## Getting Started
Cd into the cloned repo
Run npm install
Run the app with one of the sims or use the expo app to run it
