import { PodcastTypes, Sort } from '../components/types';

export const filterRss = ({ rss, loadingRss, filter }) => {
  const { sort } = filter;
  const filteredRss =
    !loadingRss &&
    rss &&
    rss.filter(item => {
      const { type, starred } = item;
      if (type === PodcastTypes.FUN && !filter.fun) return null;
      if (type === PodcastTypes.LESSON && !filter.lessons) return null;
      if (starred && filter.starred) return item;
      return item;
    });
  return sort === Sort.ASC ? filteredRss : [...filteredRss].reverse();
};
