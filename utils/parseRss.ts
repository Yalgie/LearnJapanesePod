import moment from 'moment';
import { MimeTypes, Podcast, PodcastTypes } from '../components/types';

const formatBytes = (bytes, decimals = 2): string => {
  if (bytes === 0) return '0 Bytes';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
};

export const parsePodcastRss = (rss): Array<Podcast> => {
  const parsedFeed = rss.items.map(item => {
    const { title, enclosures, published, itunes } = item;
    const { mimeType, url, length } = enclosures[0];
    const { subtitle, duration } = itunes;
    const stripped = title.toLowerCase().replace(/\s/g, '');

    if (mimeType === MimeTypes.PDF) {
      const number = stripped.match(/\d+/g).map(Number)[0];
      return {
        id: PodcastTypes.PDF + number,
        type: PodcastTypes.PDF,
        lessonRef: number,
        title,
        url,
        size: formatBytes(length),
        published: moment(published).format('YYYY/MM/DD'),
        description: subtitle,
        downloaded: false,
        completed: 0,
      };
    }
    if (mimeType === MimeTypes.AUDIO) {
      if (stripped.includes('dialog') || stripped.includes('drill')) {
        const number = stripped.match(/\d+/g).map(Number)[0];
        return {
          id: PodcastTypes.DRILL + number,
          type: PodcastTypes.DRILL,
          lessonRef: number,
          title,
          url,
          published: moment(published).format('YYYY/MM/DD'),
          duration,
          starred: false,
          size: formatBytes(length),
          description: subtitle,
          downloaded: false,
          completed: 0,
        };
      }
      if (stripped.includes('funfriday')) {
        const publishedDate = moment(published).format('YYYY/MM/DD');
        return {
          id: PodcastTypes.FUN + publishedDate,
          type: PodcastTypes.FUN,
          title,
          url,
          published: publishedDate,
          duration,
          starred: false,
          size: formatBytes(length),
          description: subtitle,
          downloaded: false,
          completed: 0,
        };
      }

      const number = stripped.match(/\d+/g).map(Number)[0];
      return {
        id: PodcastTypes.LESSON + number,
        type: PodcastTypes.LESSON,
        lessonRef: number,
        title,
        url,
        published: moment(published).format('YYYY/MM/DD'),
        duration,
        description: subtitle,
        downloaded: false,
        starred: false,
        size: formatBytes(length),
        drill: undefined,
        notes: undefined,
        completed: 0,
      };
    }
  });

  const lessons = parsedFeed.filter(item => item.type === PodcastTypes.LESSON);

  parsedFeed.forEach(item => {
    if (item.type === PodcastTypes.DRILL) {
      lessons.find(lesson => lesson.lessonRef === item.lessonRef).drill = item;
    } else if (item.type === PodcastTypes.PDF) {
      lessons.find(lesson => lesson.lessonRef === item.lessonRef).notes = item;
    }
  });

  const fun = parsedFeed.filter(item => item.type === PodcastTypes.FUN);

  return [...lessons, ...fun]
    .sort((a, b) => moment(a.published).diff(moment(b.published)))
    .reverse();
};
