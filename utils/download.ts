import { Podcast, PodcastTypes } from '../components/types';
import * as FileSystem from 'expo-file-system';

const downloadCallback = downloadProgress => {
  const progress =
    downloadProgress.totalBytesWritten /
    downloadProgress.totalBytesExpectedToWrite;
  console.log(progress);
};

export const handleDownload = async (url: string, id: string) => {
  const { rss } = this.state;
  let item: Podcast | undefined = undefined;

  rss.filter(podcast => {
    if (podcast.id === id) {
      item = podcast;
    }
    if (podcast.notes && podcast.notes.id === id) {
      item = podcast.notes;
    }
    if (podcast.drill && podcast.drill.id === id) {
      item = podcast.drill;
    }
  });

  const { type, lessonRef } = item;
  const ext = type === PodcastTypes.PDF ? '.pdf' : '.mp3';
  const filename = `podcast_${lessonRef}_${type.toLocaleLowerCase()}${ext}`;
  const downloadResumable = FileSystem.createDownloadResumable(
    url,
    FileSystem.documentDirectory + 'downloads/' + filename,
    {},
    this.downloadCallback
  );

  try {
    const { uri } = await downloadResumable.downloadAsync();
    console.log('Finished downloading to ', uri);

    const updatedRss = rss.map(it => {
      if (it.id === id) {
        it.downloaded = uri;
      }
      if (it.notes && it.notes.id === id) {
        it.notes.downloaded = uri;
      }
      if (it.drill && it.drill.id === id) {
        it.drill.downloaded = uri;
      }

      return it;
    });

    this.setState(
      {
        ...this.state,
        rss: updatedRss,
      },
      () => {
        this._storeData();
      }
    );
  } catch (e) {
    console.error(e);
  }

  // FileSystem.downloadAsync(url, FileSystem.documentDirectory + 'mp3')
  //   .then(({ uri }) => {
  //     console.log('Finished downloading to ', uri);
  //   })
  //   .catch(error => {
  //     console.error(error);
  //   });
};
