import * as FileSystem from 'expo-file-system';

export const readDir = async (dirName: string) => {
  const appDir = FileSystem.documentDirectory;

  try {
    await FileSystem.readDirectoryAsync(appDir + dirName);
    return true;
  } catch (e) {
    return false;
  }
};

export const makeDir = async (dirName: string) => {
  const appDir = FileSystem.documentDirectory;

  try {
    await FileSystem.makeDirectoryAsync(appDir + dirName);
    return true;
  } catch (e) {
    return false;
  }
};

export const deleteDir = async (dirName: string) => {
  const appDir = FileSystem.documentDirectory;

  try {
    await FileSystem.deleteAsync(appDir + dirName);
    return true;
  } catch (e) {
    return false;
  }
};
