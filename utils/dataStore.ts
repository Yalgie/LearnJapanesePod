import { AsyncStorage } from 'react-native';

export const storeData = async (key: string, data: string) => {
  try {
    await AsyncStorage.setItem(key, data);
  } catch {}
};

export const retrieveData = async (key: string) => {
  try {
    const data = await AsyncStorage.getItem(key);
    if (data !== null) {
      return JSON.parse(data);
    }
    return false;
  } catch (error) {
    return false;
  }
};

// const keys = await AsyncStorage.getAllKeys();
export const deleteRssCache = async (key: string) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch {}
};
