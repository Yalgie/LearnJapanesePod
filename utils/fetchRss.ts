import * as rssParser from 'react-native-rss-parser';
import { parsePodcastRss } from './parseRss';

export const fetchRss = async (url: string): rssParser.model => {
  return await fetch(url)
    .then(response => response.text())
    .then(responseData => rssParser.parse(responseData))
    .then(rss => parsePodcastRss(rss));
};
