import React, { useState } from 'react';
import styled from 'styled-components';
import { View } from 'react-native';
import { PodcastHeader } from './PodcastHeader';
import { PodcastTabs, Tabs } from './PodcastTabs';
import { PodcastTabContent } from './PodcastTabContent';
import { Podcast, PodcastTypes } from '../types';

const Wrap = styled(View)`
  margin-top: 24px;
`;

type Props = {
  podcast: Podcast;
  playNewAudio: (src: string) => void;
  handleDownload: (src: string, id: string) => void;
  handleStar: (starred: boolean, id: string) => void;
  handlePdf: (podcast: Podcast) => void;
};

export const PodcastWrapper = ({
  podcast,
  playNewAudio,
  handleDownload,
  handleStar,
  handlePdf,
}: Props) => {
  const { drill, notes, lessonRef, type, starred, id } = podcast;
  const [activeTab, setActiveTab] = useState(Tabs.LESSON);

  const handleTabPress = (tab: Tabs): void => setActiveTab(tab);

  const getTabContent = (): Podcast => {
    if (activeTab === Tabs.LESSON) {
      return podcast;
    }

    if (activeTab === Tabs.DRILL && drill) {
      return drill;
    }

    if (activeTab === Tabs.PDF && notes) {
      return notes;
    }
  };

  const tabContent = getTabContent();
  const title =
    type === PodcastTypes.FUN ? 'Fun Friday' : 'Podcast ' + lessonRef;

  return (
    <Wrap>
      <PodcastHeader
        title={title}
        starred={starred}
        handleStar={handleStar}
        id={id}
      />
      {type !== PodcastTypes.FUN && (
        <PodcastTabs
          activeTab={activeTab}
          hasDrill={!!drill}
          hasNotes={!!notes}
          handleTabPress={handleTabPress}
        />
      )}
      <PodcastTabContent
        content={tabContent}
        playNewAudio={playNewAudio}
        handleDownload={handleDownload}
        handlePdf={handlePdf}
      />
    </Wrap>
  );
};
