import React from 'react';
import styled, { css } from 'styled-components';
import { Text, View } from 'react-native';

const TabsWrapper = styled(View)`
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  border-bottom-width: 1px;
  border-color: #ddd;
`;

const TabButton = styled.TouchableOpacity`
  width: 33.33%;
`;

const activeCSS = css`
  background-color: #ddd;
`;

const defaultCSS = css`
  background-color: #ececec;
`;

const Tab = styled.View`
  ${props => (props.active ? activeCSS : defaultCSS)};
  padding-vertical: 12px;
  padding-horizontal: 8px;
  border-top-left-radius: 8px;
  border-top-right-radius: 8px;
`;

const TabText = styled(Text)`
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 10px;
`;

export enum Tabs {
  LESSON = 'Lesson',
  DRILL = 'Dialogue',
  PDF = 'Notes',
}

type Props = {
  activeTab: Tabs;
  handleTabPress: (tab: Tabs) => void;
  hasNotes: boolean;
  hasDrill: boolean;
};

export const PodcastTabs = ({
  activeTab,
  handleTabPress,
  hasDrill,
  hasNotes,
}: Props) => {
  return (
    <TabsWrapper>
      <TabButton onPress={(): void => handleTabPress(Tabs.LESSON)}>
        <Tab active={activeTab === Tabs.LESSON}>
          <TabText>{Tabs.LESSON}</TabText>
        </Tab>
      </TabButton>
      {hasDrill && (
        <TabButton onPress={(): void => handleTabPress(Tabs.DRILL)}>
          <Tab active={activeTab === Tabs.DRILL}>
            <TabText>{Tabs.DRILL}</TabText>
          </Tab>
        </TabButton>
      )}
      {hasNotes && (
        <TabButton onPress={(): void => handleTabPress(Tabs.PDF)}>
          <Tab active={activeTab === Tabs.PDF}>
            <TabText>{Tabs.PDF}</TabText>
          </Tab>
        </TabButton>
      )}
    </TabsWrapper>
  );
};
