import React from 'react';
import styled from 'styled-components';
import { Text, View } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';

const Wrap = styled(View)`
  margin-bottom: 4px;
`;

const Header = styled.View`
  display: flex;
  flex-flow: row;
  align-items: center;
`;

const HeaderText = styled(Text)`
  font-size: 20px;
  margin-left: 8px;
`;

const FavouriteButton = styled.TouchableOpacity``;

type Props = {
  title: string;
  starred: boolean;
  handleStar: (starred: boolean, id: string) => void;
  id: string;
};

export const PodcastHeader = ({ title, starred, handleStar, id }: Props) => {
  return (
    <Wrap>
      <Header>
        <FavouriteButton onPress={() => handleStar(!starred, id)}>
          <FontAwesome
            name="star"
            size={20}
            style={{ color: starred ? '#3cd070' : '#DDD' }}
          />
        </FavouriteButton>
        <HeaderText>{title}</HeaderText>
      </Header>
    </Wrap>
  );
};
