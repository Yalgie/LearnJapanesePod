import React from 'react';
import styled, { css } from 'styled-components';
import { Text, View } from 'react-native';
import { Podcast, PodcastTypes } from '../../types';
import { FontAwesome } from '@expo/vector-icons';

const fullBorderRadiusCSS = css`
  border-radius: 8px;
`;

const bottomBorderRadiusCSS = css`
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;
`;

const Button = styled.TouchableOpacity``;

const Wrap = styled(View)`
  background-color: #f1f1f1;
  ${props =>
    props.type === PodcastTypes.FUN
      ? fullBorderRadiusCSS
      : bottomBorderRadiusCSS}
`;

const ContentWrap = styled.View`
  padding: 8px;
`;

const Title = styled(Text)`
  font-size: 16px;
  margin-bottom: 4px;
`;

const Description = styled(Text)``;

const FooterButtons = styled.View`
  ${bottomBorderRadiusCSS};
  padding: 8px;
  display: flex;
  flex-flow: row;
  justify-content: space-between;
  align-items: center;
  border-top-width: 1px;
  border-color: #ddd;
`;

const FooterButton = styled.TouchableOpacity``;

const ButtonText = styled(Text)`
  text-align: center;
  text-transform: uppercase;
  font-weight: bold;
  font-size: 10px;
`;

const DetailsWrap = styled.View`
  display: flex;
  flex-flow: row;
  align-items: center;
`;

const DetailsText = styled(Text)`
  color: #333;
  font-size: 12px;
  padding-horizontal: 4px;
`;

type Props = {
  content: Podcast;
  playNewAudio: (src: string) => void;
  handleDownload: (src: string, id: string) => void;
  handlePdf: (podcast: Podcast) => void;
};

export const PodcastTabContent = ({
  content,
  playNewAudio,
  handleDownload,
  handlePdf,
}: Props) => {
  const {
    type,
    title,
    lessonRef,
    description,
    url,
    published,
    duration,
    downloaded,
    id,
    size,
  } = content;

  const handlePress = () => {
    if (type === PodcastTypes.PDF) {
      handlePdf(content);
    } else if (downloaded && typeof downloaded === 'string') {
      playNewAudio(downloaded);
    } else {
      playNewAudio(url);
    }
  };

  const onDownload = () => {
    handleDownload(url, id);
  };

  const titleText =
    type === PodcastTypes.FUN
      ? title.split('riday: ')[1]
      : title.split(`${lessonRef}: `)[1];
  const sentences =
    type === PodcastTypes.DRILL ? 'View Example Sentences' : null;
  const icon = type === PodcastTypes.PDF ? 'external-link' : 'play-circle';
  return (
    <Wrap type={type}>
      <ContentWrap>
        <Title>{titleText}</Title>
        <Description>{description}</Description>
        {sentences && (
          <Button>
            <Text>{sentences}</Text>
          </Button>
        )}
      </ContentWrap>
      <FooterButtons>
        <FooterButton onPress={handlePress}>
          <ButtonText>
            <FontAwesome name={icon} size={20} style={{ color: '#333' }} />
          </ButtonText>
        </FooterButton>
        <DetailsWrap>
          <DetailsText>{published}</DetailsText>
          <FontAwesome name="circle" size={5} style={{ color: '#ccc' }} />
          {duration && (
            <>
              <DetailsText>{duration}</DetailsText>
              <FontAwesome name="circle" size={5} style={{ color: '#ccc' }} />
            </>
          )}
          <DetailsText>{size}</DetailsText>
        </DetailsWrap>
        <FooterButton onPress={onDownload}>
          <ButtonText>
            <FontAwesome
              name="download"
              size={20}
              style={{ color: downloaded ? '#3cd070' : '#333' }}
            />
          </ButtonText>
        </FooterButton>
      </FooterButtons>
    </Wrap>
  );
};
