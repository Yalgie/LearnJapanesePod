import React from 'react';
import { SafeAreaView, FlatList } from 'react-native';
import styled from 'styled-components';

import { PodcastTypes, Podcast, Sort } from '../types';
import { PodcastWrapper } from '../Podcast';

const SafeWrap = styled(SafeAreaView)`
  flex: 1;
  background-color: #fff;
`;

const Wrap = styled.View`
  padding-horizontal: 16px;
  background-color: #fff;
  flex: 1;
`;

export const Home = ({
  loadingRss,
  rss,
  handleRssRefresh,
  playNewAudio,
  handleDownload,
  handleStar,
  navigation,
}) => {
  const renderItem = ({ item }) => {
    return (
      <PodcastWrapper
        podcast={item}
        playNewAudio={playNewAudio}
        handleDownload={handleDownload}
        handlePdf={(podcast: Podcast) =>
          navigation.navigate('PDF', {
            podcast,
          })
        }
        handleStar={handleStar}
      />
    );
  };

  return (
    <Wrap>
      {!loadingRss && rss && (
        <FlatList
          data={rss}
          renderItem={renderItem}
          keyExtractor={(item: Podcast): string => item.id}
          refreshing={false}
          onRefresh={handleRssRefresh}
          showsVerticalScrollIndicator={false}
        />
      )}
    </Wrap>
  );
};
