import React from 'react';
import { Text, View } from 'react-native';
import PDFReader from 'rn-pdf-reader-js'

export const PDF = ({ route }) => {
  const { podcast } = route.params;
  const { url, downloaded } = podcast;

  return (
    <PDFReader
      source={{
        uri: downloaded || url,
      }}
    />
  );
};
