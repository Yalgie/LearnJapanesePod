import React from 'react';
import { FontAwesome } from '@expo/vector-icons';
import { Home } from '../Home';
import { Settings } from '../Settings';
import { PDF } from '../Pdf';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import styled from 'styled-components';

const FilterButton = styled.TouchableOpacity`
  padding-horizontal: 16px;
`;

export const Nav = ({
  filteredRss,
  loadingRss,
  handleRssRefresh,
  playNewAudio,
  handleDownload,
  handleStar,
  handleFilter,
}) => {
  const Stack = createStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen
          name="Home"
          options={({ navigation }) => ({
            title: 'Home',
            headerLeft: () => (
              <FilterButton onPress={handleFilter}>
                <FontAwesome
                  name="filter"
                  size={20}
                  style={{ color: '#333' }}
                />
              </FilterButton>
            ),
            headerRight: () => (
              <FilterButton onPress={() => navigation.navigate('Settings')}>
                <FontAwesome name="cog" size={20} style={{ color: '#333' }} />
              </FilterButton>
            ),
          })}>
          {props => (
            <Home
              {...props}
              rss={filteredRss}
              loadingRss={loadingRss}
              handleRssRefresh={handleRssRefresh}
              playNewAudio={playNewAudio}
              handleDownload={handleDownload}
              handleStar={handleStar}
            />
          )}
        </Stack.Screen>
        <Stack.Screen name="Settings" component={Settings} />
        <Stack.Screen name="PDF" component={PDF} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
