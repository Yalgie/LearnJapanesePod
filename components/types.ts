import * as rssParser from 'react-native-rss-parser';

export enum MimeTypes {
  AUDIO = 'audio/mpeg',
  PDF = 'application/pdf',
}

export enum PodcastTypes {
  LESSON = 'LESSON',
  PDF = 'PDF',
  DRILL = 'Dialogue',
  FUN = 'FUN FRIDAY',
}

export type Podcast = {
  id: string;
  type: PodcastTypes;
  lessonRef: number;
  title: string;
  url: string;
  published: string;
  completed: number;
  duration?: string;
  description: string;
  downloaded: boolean | string;
  starred?: boolean;
  size: string | undefined;
  drill?: Podcast;
  notes?: Podcast;
};

export enum Sort {
  ASC = 'ASC',
  DESC = 'DESC',
}

export type Filter = {
  fun: boolean;
  lessons: boolean;
  starred: boolean;
  sort: Sort;
};

export type AppState = {
  rss: undefined | Array<Podcast>;
  loadingRss: boolean;
  announcements: undefined | Array<Podcast>;
  loadingRssAnnouncements: boolean;
  filter: Filter;
  audio: string | null;
};

export type AudioState = {
  loadingAudio: boolean;
  isPlaying: boolean;
  isBuffering: boolean;
  isLoaded: boolean;
  isLooping: boolean;
  isMuted: boolean;
  rate: number;
  duration: number;
  uri: null | string;
  volume: number;
};

export enum DataStore {
  RSS = 'rss',
  FILTER = 'filter',
}
