import React, { useState } from 'react';
import { useKeepAwake } from 'expo-keep-awake';
import { Audio } from 'expo-av';
import { AudioPlayerComponent } from './component';

type Props = {
  audio: string | null;
};

const soundObject = new Audio.Sound();

export const AudioPlayer = ({ audio }: Props) => {
  useKeepAwake();
  const [isPlaying, setIsPlaying] = useState(false);
  const [isBuffering, setIsBuffering] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [isLooping, setIsLooping] = useState(false);
  const [isMuted, setIsMuted] = useState(false);
  const [rate, setRate] = useState(false);
  const [duration, setDuration] = useState(false);
  const [volume, setVolume] = useState(false);
  const [uri, setUri] = useState('');
  const [fetchingAudio, setFetchingAudio] = useState(false);

  // changeRate = async () => {
  //   await this.soundObject.setRateAsync(2, false, null);
  // };
  //
  const _onPlaybackStatusUpdate = playbackStatus => {
    if (!playbackStatus.isLoaded) {
      // Update your UI for the unloaded state
      if (playbackStatus.error) {
        console.log(
          `Encountered a fatal error during playback: ${playbackStatus.error}`
        );
        // Send Expo team the error on Slack or the forums so we can help you debug!
      }
    } else {
      // Update your UI for the loaded state

      if (playbackStatus.isPlaying) {
        setIsPlaying(true);
      } else {
        setIsPlaying(false);
      }

      if (playbackStatus.isBuffering) {
        setIsBuffering(true);
      } else {
        setIsBuffering(false);
      }

      if (playbackStatus.didJustFinish && !playbackStatus.isLooping) {
        // The player has just finished playing and will stop. Maybe you want to play something else?
      }
    }
  };

  const playNewAudio = async (src: string) => {
    if (isPlaying) {
      await soundObject.stopAsync();
      await soundObject.unloadAsync();
    }

    try {
      setFetchingAudio(true);

      await soundObject.loadAsync(
        {
          uri: src,
        },
        {},
        false
      );

      await soundObject.playAsync();

      setFetchingAudio(false);
    } catch (e) {}
  };

  const playPause = async () => {
    if (isPlaying) {
      await soundObject.pauseAsync();
    } else {
      await soundObject.playAsync();
    }
  };

  if (audio !== uri && typeof audio === 'string') {
    setUri(audio);
    playNewAudio(audio);
  }

  soundObject.setOnPlaybackStatusUpdate(_onPlaybackStatusUpdate);

  return (
    !!uri && (
      <AudioPlayerComponent
        isPlaying={isPlaying}
        isBuffering={isBuffering}
        loadingAudio={fetchingAudio}
        handlePlayPause={playPause}
      />
    )
  );
};
