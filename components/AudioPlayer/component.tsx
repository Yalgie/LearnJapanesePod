import React, { useState } from 'react';
import { FontAwesome } from '@expo/vector-icons';
import styled from 'styled-components';
import { View, TouchableOpacity, Animated } from 'react-native';

const Controls = styled(View)`
  background-color: #f1f1f1;
  display: flex;
  flex-flow: row;
  align-items: center;
  justify-content: space-between;
  padding: 16px;
  border-top-width: 1px;
  border-color: #333;
`;

const FF30 = styled(FontAwesome)`
  transform: rotateY(180deg);
`;

const RW30 = styled(FontAwesome)``;

const BufferLoad = styled(FontAwesome)``;

type Props = {
  isPlaying: boolean;
  isBuffering: boolean;
  loadingAudio: boolean;
  handlePlayPause: () => void;
};

type State = {
  rotateValue: any;
};

export const AudioPlayerComponent = ({
  isPlaying,
  isBuffering,
  loadingAudio,
  handlePlayPause,
}: Props) => {
  const [rotateAnim] = useState(new Animated.Value(0));

  React.useEffect(() => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(rotateAnim, {
          toValue: 1,
          duration: 1250,
        }),
      ]),
      {
        iterations: 30,
      }
    ).start();
  }, []);

  return (
    <>
      {/*<FontAwesome name="check-circle" size={25} style={{ color: 'red' }} />*/}
      {/*<FontAwesome name="download" size={25} style={{ color: 'red' }} />*/}
      {/*<FontAwesome name="bars" size={25} style={{ color: 'red' }} />*/}
      {/*<FontAwesome name="pause-circle" size={25} style={{ color: 'red' }} />*/}
      {/*<FontAwesome name="retweet" size={25} style={{ color: 'red' }} />*/}
      {/*<FontAwesome name="random" size={25} style={{ color: 'red' }} />*/}

      <Controls>
        <RW30 name="undo" size={25} style={{ color: '#333' }} />

        <FontAwesome name="step-backward" size={25} style={{ color: '#333' }} />

        {!isPlaying && (isBuffering || loadingAudio) && (
          <Animated.View
            style={{
              transform: [
                {
                  rotate: rotateAnim.interpolate({
                    inputRange: [0, 1],
                    outputRange: ['0deg', '360deg'],
                  }),
                },
              ],
            }}>
            <BufferLoad name="spinner" size={50} style={{ color: '#333' }} />
          </Animated.View>
        )}

        {!isPlaying && !isBuffering && !loadingAudio && (
          <TouchableOpacity onPress={handlePlayPause}>
            <FontAwesome
              name="play-circle"
              size={50}
              style={{ color: '#3cd070' }}
            />
          </TouchableOpacity>
        )}

        {isPlaying && !isBuffering && !loadingAudio && (
          <TouchableOpacity onPress={handlePlayPause}>
            <FontAwesome
              name="pause-circle"
              size={50}
              style={{ color: '#3cd070' }}
            />
          </TouchableOpacity>
        )}

        <FontAwesome name="step-forward" size={25} style={{ color: '#333' }} />

        <FF30 name="undo" size={25} style={{ color: '#333' }} />
      </Controls>
    </>
  );
};
